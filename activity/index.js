const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 5001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
  "mongodb+srv://kendrick123:kendrick123@cluster0.nkwttx3.mongodb.net/?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

let db = mongoose.connection;
//error handling
db.on("error", console.error.bind(console, "Conn error"));
db.on("open", () => console.log("Connected to database"));

const userSchema = new mongoose.Schema({
  username: String,
  password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }, (err, result) => {
    if (result != null && result.username == req.body.username) {
      return res.send("User already exists");
    } else {
      let newUser = new User({
        username: req.body.username,
        password: req.body.password
      });
      newUser.save((saveErr, savedUser) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New User created");
        }
      });
    }
  });
});

app.listen(port, () => console.log(`Server running at port ${port}`));
